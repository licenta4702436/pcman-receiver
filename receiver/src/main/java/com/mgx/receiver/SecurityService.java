package com.mgx.receiver;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SecurityService {
    @Value("#{environment.OWNER_IP_ADDRESS}")
    private String OWNER_IP_ADDRESS;

    public void assertAccess() {
        //TODO
    }
}
