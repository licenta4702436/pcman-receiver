package com.mgx.receiver;

import lombok.Data;

@Data
public class DeleteFilePayload {
    private String path;
}
