package com.mgx.receiver;

import com.mgx.receiver.helpers.DedicatedFile;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/main")
@RequiredArgsConstructor
public class SystemManagementController {
    private final SystemManagementService service;

    @GetMapping("/ping")
    public String ping() {
        return "Deschis";
    }

    @PostMapping("/command")
    public String execute(@RequestBody CommandPayload payload) throws IOException, InterruptedException {
        return service.execute(payload);
    }

    @PostMapping("/shutdown")
    public void shutdown() throws IOException, InterruptedException {
        service.shutdown();
    }

    @PostMapping("/restart")
    public void restart() throws IOException, InterruptedException {
        service.restart();
    }

    @GetMapping("/dedicated-file-system")
    public DedicatedFile getDedicatedFileSystem() {
        return service.getDedicatedFileSystem();
    }

    @PostMapping("/dedicated-file-system")
    public void createFileInDedicatedFileSystem(@RequestBody CreateFilePayload payload) throws IOException {
        service.createFileInDedicatedFileSystem(payload);
    }

    @PostMapping("/dedicated-file-system/delete")
    public void deleteFileInDedicatedFileSystem(@RequestBody DeleteFilePayload payload) throws IOException {
        service.deleteFileInDedicatedFileSystem(payload);
    }

    @PostMapping("/screenshot")
    public void takeScreenshot(HttpServletResponse response) throws IOException, AWTException {
        service.takeScreenshot(response);
    }

    @PostMapping("/webcam")
    public void takeWebcamPhoto(HttpServletResponse response) throws IOException {
        service.takeWebcamPhoto(response);
    }
}
