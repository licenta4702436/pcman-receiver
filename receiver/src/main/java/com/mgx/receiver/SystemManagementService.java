package com.mgx.receiver;

import com.github.sarxos.webcam.Webcam;
import com.mgx.receiver.helpers.DedicatedFile;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class SystemManagementService {
    private static final String OPERATING_SYSTEM = System.getProperty("os.name");

    private final CommandBuilderService commandBuilderService;
    private final SecurityService securityService;

    public String execute(CommandPayload payload) throws IOException, InterruptedException {
        securityService.assertAccess();
        ProcessBuilder pb = commandBuilderService.getExecuteProcess(OPERATING_SYSTEM, payload);
        Process process = pb.start();

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }
        process.waitFor();

        return builder.toString();
    }

    public void shutdown() throws IOException, InterruptedException {
        securityService.assertAccess();
        ProcessBuilder pb = commandBuilderService.getShutdownProcess(OPERATING_SYSTEM);
        Process process = pb.start();

//        process.waitFor();

        System.exit(0);
    }

    public void restart() throws IOException, InterruptedException {
        securityService.assertAccess();
        ProcessBuilder pb = commandBuilderService.getRestartProcess(OPERATING_SYSTEM);
        Process process = pb.start();

//        process.waitFor();

        System.exit(0);
    }

    public DedicatedFile getDedicatedFileSystem() {
        securityService.assertAccess();
        File dedicatedDirectory = new File("C:\\pcman_dedicated");

        DedicatedFile dedicatedFile = new DedicatedFile().setName(dedicatedDirectory.getName()).setPath(dedicatedDirectory.getAbsolutePath()).setIsDirectory(dedicatedDirectory.isDirectory());
        getFiles(dedicatedDirectory, dedicatedFile);

        return dedicatedFile;
    }

    private void getFiles(File file, DedicatedFile dedicatedFile) {
        if (file.isDirectory()) {
            for (File file1 : Objects.requireNonNull(file.listFiles())) {
                DedicatedFile newfs = new DedicatedFile().setName(file1.getName()).setPath(file1.getAbsolutePath()).setIsDirectory(file1.isDirectory());
                dedicatedFile.addChild(newfs);
                getFiles(file1, newfs);
            }
        }
    }

    public void createFileInDedicatedFileSystem(CreateFilePayload payload) throws IOException {
        securityService.assertAccess();
        Path newFilePath = Paths.get(payload.getPath());
        Files.createFile(newFilePath);
    }

    public void deleteFileInDedicatedFileSystem(DeleteFilePayload payload) throws IOException {
        securityService.assertAccess();
        Path oldFilePath = Paths.get(payload.getPath());
        Files.deleteIfExists(oldFilePath);
    }

    public void takeScreenshot(HttpServletResponse response) throws AWTException, IOException {
        Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage capture = new Robot().createScreenCapture(screenRect);

        response.setContentType("image/png");
        ImageIO.write(capture, "png", response.getOutputStream());
    }

    public void takeWebcamPhoto(HttpServletResponse response) throws IOException {
        Webcam webcam = Webcam.getDefault();
        webcam.open();

        BufferedImage image = webcam.getImage();
        webcam.close();

        response.setContentType("image/png");
        ImageIO.write(image, "png", response.getOutputStream());
    }
}
