package com.mgx.receiver;

import lombok.Data;

@Data
public class CommandPayload {
    private String command;
}
