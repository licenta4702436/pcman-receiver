package com.mgx.receiver;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommandBuilderService {
    public ProcessBuilder getShutdownProcess(String operatingSystem) {
        String command = "shutdown";
        String argument;
        String timer;

        if ("Linux".equals(operatingSystem) || "Mac OS X".equals(operatingSystem)) {
            argument = "-h";
            timer = "now";
        }
        else if (operatingSystem.contains("Windows")) {
            argument = "-s";
            timer = "-t";
        }
        else {
            throw new RuntimeException("Unsupported operating system.");
        }

        return new ProcessBuilder(command, argument, timer, "5");
    }

    public ProcessBuilder getRestartProcess(String operatingSystem) {
        String command;
        String argument;
        String timer;

        if ("Linux".equals(operatingSystem) || "Mac OS X".equals(operatingSystem)) {
            command = "shutdown";
            argument = "-r";
            timer = "now";
        }
        else if (operatingSystem.contains("Windows")) {
            command = "shutdown.exe";
            argument = "-r";
            timer = "-t";
        }
        else {
            throw new RuntimeException("Unsupported operating system.");
        }
        return new ProcessBuilder(command, argument, timer, "5");
    }

    public ProcessBuilder getExecuteProcess(String operatingSystem, CommandPayload payload) {

        return new ProcessBuilder(payload.getCommand());
    }
}
