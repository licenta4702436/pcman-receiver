package com.mgx.receiver;

import lombok.Data;

@Data
public class CreateFilePayload {
    private String path;
}
