package com.mgx.receiver.helpers;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class DedicatedFile {
    private String name;
    private String path;
    private Boolean isDirectory;
    private List<DedicatedFile> children = new ArrayList<>();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(name);
        sb.append("\nPath: ").append(path);
        sb.append("\nIs directory: ").append(isDirectory);
        sb.append("\nChildren:\n");

        for (DedicatedFile fs : children) {
            sb.append(fs.toString()).append("\n");
        }

        return sb.toString();
    }

    public void addChild(DedicatedFile child) {
        this.children.add(child);
    }
}
